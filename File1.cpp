#include <iostream>
/*! ����� ����.

*/
class lift_t
{
	public:
		//! ������� ����.
		int m_stage;
		//! ����� ������;
		int m_max_stage;
		//! ��������� ������.
		bool m_is_open;
		//! ������������ �� �������� ����.
		void move( int stage )
		{
			if( stage > 0 && stage <= m_max_stage )
			{
				m_stage = stage;
			}
		}
		//! ����������� �� ���������.
		lift_t()
		{
			m_stage = 2;
			m_max_stage = 11;
			m_is_open = false;
			std::cout << "Constructor without parameters\n";
		}
		lift_t( int stage )
		{
			m_max_stage = 20;
			m_stage = stage;
			std::cout << "Constructor with one integer parameter\n";
		}
		lift_t( int stage, int max_stage )
		{
			m_max_stage = max_stage;
			m_stage = stage;
			std::cout << "Constructor with two parameters\n";
		}
		~lift_t(){
			std::cout << "Destruction of the object!\n";
		}

};
void display( lift_t & lift )
{
	std::cout << "Etazh����: " << lift.m_stage << std::endl;
}


int _tmain(int argc, _TCHAR* argv[])
{
	lift_t lift1;
	lift_t lift2(12);
	lift_t lift3(12, 29);
    delete lift1;
	int x = std::cin.get();
	return 0;
}
